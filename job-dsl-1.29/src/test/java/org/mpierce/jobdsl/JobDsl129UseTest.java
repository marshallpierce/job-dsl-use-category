package org.mpierce.jobdsl;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import javaposse.jobdsl.dsl.DslScriptLoader;
import javaposse.jobdsl.dsl.JobManagement;
import javaposse.jobdsl.dsl.MemoryJobManagement;
import javaposse.jobdsl.dsl.ScriptRequest;
import org.junit.Before;
import org.junit.Test;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.find;
import static org.junit.Assert.assertTrue;

public final class JobDsl129UseTest {

    private Path jobsDir;

    @Before
    public void setUp() throws Exception {
        // find root of the project
        Optional<Path> sourceFile = find(new File(".").getCanonicalFile().toPath(), 10,
                (p, attr) -> p.getFileName().toString().equals(getClass().getSimpleName() + ".java"))
                .findFirst();

        if (!sourceFile.isPresent()) {
            throw new RuntimeException("Couldn't find current source file");
        }

        // traverse up to the parent dir so we can then find the job dsl dirs
        Path parent = sourceFile.get();

        for (int i = 0; i < 7; i++) {
            parent = parent.getParent();
        }

        jobsDir = parent.resolve("jobs");
        assertTrue(jobsDir.toString(), jobsDir.toFile().exists());
    }

    @Test
    public void testWithCategory() throws IOException {
        testJobFile("withCategory.groovy");
    }

    @Test
    public void testNoCategory() throws IOException {
        testJobFile("noCategory.groovy");
    }

    private void testJobFile(String jobFile) throws IOException {
        JobManagement jm = new MemoryJobManagement();

        URL urlRoot = jobsDir.toFile().toURI().toURL();
        ScriptRequest scriptRequest =
                new ScriptRequest(null, new String(Files.readAllBytes(jobsDir.resolve(jobFile)), UTF_8),
                        urlRoot);

        DslScriptLoader.runDslEngine(scriptRequest, jm);
    }
}
